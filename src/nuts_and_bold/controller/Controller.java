package nuts_and_bold.controller;

import java.io.IOException;

import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Servlet implementation class for Servlet: Controller
 *
 */
@WebServlet(name="Controller mvc", displayName="Controller mvc", urlPatterns = {"/controller-mvc"})
 public class Controller extends javax.servlet.http.HttpServlet implements javax.servlet.Servlet {
   static final long serialVersionUID = 1L;
   
    /* (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#HttpServlet()
	 */
	public Controller() {
		super();
	}   	
	
	/* (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#doGet(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doGet(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		String action = request.getParameter("action");
		
		String page = null;
		
		if(action == null) {
			page = "/02 Nuts and Bolts/mvc/error.jsp";
		}
		else if(action.equals("about")) {
			page = "/02 Nuts and Bolts/mvc/about.jsp";
		}
		else if(action.equals("login")) {
			page = "/02 Nuts and Bolts/mvc/login.jsp";
		}
		else {
			page = "/02 Nuts and Bolts/mvc/error.jsp";
		}
		
		getServletContext().getRequestDispatcher(page).forward(request, response);
	}  	
	
	/* (non-Java-doc)
	 * @see javax.servlet.http.HttpServlet#doPost(HttpServletRequest request, HttpServletResponse response)
	 */
	protected void doPost(HttpServletRequest request, HttpServletResponse response) throws ServletException, IOException {
		// TODO Auto-generated method stub
	}   	  	    
}